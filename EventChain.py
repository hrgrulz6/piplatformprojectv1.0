'''
Created on May 5, 2017

@author: Jackfruit_01
'''
from DBData import DbData
from EventExecutor import EventExecutor

class EventChain(object):
    
    dbData = DbData()
    
    def getEventChain(self, eventData):
        
        print('eventData in getEventChain: ', eventData)
        
        eventChain = self.dbData.getEventChain()
            
        print(' eventChain in eventchain ', eventChain)
        
        eventChains = []
        
        for eachChain in eventChain:
                
            print('eachChain: ', eachChain)   
            
            for eachEvent in eventData:
            
                print('eachEvent: ', eachEvent) 
                
                if eachChain[0] == eachEvent['eventSubtype']:
                    
                    print('get event chain ', eachChain )
                    
                    eventChains.append(eachChain)
                    
        print('eventChains: ',eventChains)
        
        eventExecutor = EventExecutor()
        eventExecutor.executeActions(eventData, eventChains)
        
