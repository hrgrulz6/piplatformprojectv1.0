'''
Created on May 4, 2017

@author: Jackfruit_01
'''
import sqlite3, json
from datetime import datetime

class DbData():
    
    ''' timestamp '''
    timestamp = datetime.now()
    print('timestamp', timestamp)
    
    ''' create a connection with the PiEvents DB '''
    dbConnection = sqlite3.connect('PiEvents.db')
        
    dbCursor = dbConnection.cursor()
        
    print('db connection created ')
    
    def getEventScheduleAndStatus(self):
        
        eventsData = self.dbCursor.execute('SELECT * FROM SchedulerEvent')
        
        print('eventsData: ', eventsData)
        '''for eachRowData in eventsData:
            print('eachRowData in from DB: ', eachRowData)
            break'''
        return eventsData
    
    def updateLastPostTimeToDB(self, currentTime, subType, eventType):
        
        print('updateLastPostTimeToDB',currentTime, subType, eventType)
        
        self.dbCursor.execute('UPDATE SchedulerEvent SET LastPostedTime = ? WHERE EventSubType = ? AND EventType=?',
                              (currentTime, subType, eventType))
        
        self.dbConnection.commit()
        
        print('updatedTime: ', currentTime)
        
        return 'updated'
    
    def getEventChain(self):
        
        eventChain = self.dbCursor.execute('SELECT * FROM EventsChain')
        
        print('eventChain in getEventChain', eventChain)
        
        return eventChain
    
    def insertEventToDB(self, data):
        
        print('data in dbdata: ', data)
        
        #jsonData = json.loads(data)
        
        #print('jsonData in dbdata: ', jsonData["eventData"])
        
        self.dbCursor.execute("INSERT INTO EventsStore VALUES (?, ?, ?, ?)", 
                              (data['eventType'], data['eventSubType'], data['timestamp'], str(data['eventData'])))
        
        self.dbConnection.commit()
        
        return self.timestamp
    
    def updateLastCollectTimeToDB(self, currentTime, subType, eventType):
        
        print('updateLastCollectTimeToDB',currentTime, subType, eventType)
        
        self.dbCursor.execute('UPDATE SchedulerEvent SET LastCollectedTime = ? WHERE EventSubType = ? AND EventType=?',
                              (currentTime, subType, eventType))
        
        self.dbConnection.commit()
        
        print('updatedTime: ', currentTime)
        
        return 'updated'
        
        
        
        
        
        
        