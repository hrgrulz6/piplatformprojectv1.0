'''
Created on May 8, 2017

@author: Jackfruit_01
'''
import sqlite3

''' create a connection with the PiEvents DB '''
dbConnection = sqlite3.connect('PiEvents.db')

dbCursor = dbConnection.cursor()

dbCursor.execute('''CREATE TABLE IF NOT EXISTS EventsStore
            (EventType text, EventSubType text, Timestamp text, Data text)''')

dbConnection.commit()

print('EventsStore table created in DB')