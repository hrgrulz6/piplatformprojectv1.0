'''
Created on May 6, 2017

@author: Jackfruit_01
'''
from DBData import DbData
from CaptureData import Capturedata
from KafkaProducer import Kafka_Producer

class EventExecutor(object):
    
    dbData = DbData()
    capturedata = Capturedata()
    kafka_Producer = Kafka_Producer()
    
    def executeActions(self, eventData, eventChains):
        
        print('eventData in executeActions', eventData)
        print('eventChains in executeActions', eventChains)
        
        for eachChain in eventChains:
            
            for eachEvent in eventData:
                
                if eachEvent['eventSubtype'] == 'driverLog':
                    
                    print('driverLog', eachEvent)
                    logInfo = self.capturedata.getLogData(eachEvent)
                    insertTime = self.dbData.insertEventToDB(logInfo)
                    print('insertTime toCollect ', insertTime)
                    self.dbData.updateLastCollectTimeToDB(insertTime, logInfo.get('eventSubType'), logInfo.get('eventType'))
                    
                    response = self.kafka_Producer.postEventToSpark(logInfo)
                    print('dataPosted: ',response)
                        
                    if response['kafkaResponse'][0] == 'streamTest':
                            
                        self.dbData.updateLastPostTimeToDB(response['time'], logInfo.get('eventSubType'), logInfo.get('eventType'))
                else:
                
                    if eachChain[0] == eachEvent['eventSubtype']:
                
                        print('get: ', eachEvent['eventSubtype'])
                    
                        data = self.capturedata.getData(eachChain[1])
                        print('data: ', data )
                    
                        if eachEvent['toCollect'] == 'true':
                        
                            print('execute toCollect: ', eachChain[1])
                        
                            time = self.dbData.insertEventToDB(data)
                            print('time toCollect ', time)
                            self.dbData.updateLastCollectTimeToDB(time, data['eventSubType'], data['eventType'])
                        else:
                            print('nothing to collect')
                        
                        if eachEvent['toPost'] == 'true':
                        
                            print('execute toPost: ', eachChain[1])
                        
                            response = self.kafka_Producer.postEventToSpark(data)
                            print('dataPosted: ',response)
                        
                            if response['kafkaResponse'][0] == 'streamTest':
                            
                                self.dbData.updateLastPostTimeToDB(response['time'], data['eventSubType'], data['eventType'])
                        
    
    
    
            
                
            
        
        