'''
Created on May 5, 2017

@author: Jackfruit_01
'''
from EventReceiver import EventReceiver

class EventProcessor(object):
    
    eventReceiver = EventReceiver()
    
    def processEvent(self, eventData):
        
        print('eventData in processEvent: ', eventData)
        
        self.eventReceiver.receiveEvent(eventData)