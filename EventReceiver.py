'''
Created on May 5, 2017

@author: Jackfruit_01
'''
from EventChain import EventChain

class EventReceiver(object):
    
    eventChain = EventChain()
    
    def receiveEvent(self, eventData):
        
        print('eventData in receiveEvent: ', eventData)
        
        self.eventChain.getEventChain(eventData)
        
        return 'done'