'''
Created on May 6, 2017

@author: Jackfruit_01
'''

class Diagnostics(object):
    
    
    def getDiagnostics(self):
        
        diagnosticsInfo = {#"type": "diagnostics", 
                    #"subType": "engineStatistics", 
                    "id": 1,
                    "name": "DIGI",
                    "timestamp": "2017-04-18 09:58:00",
                    'fuelLevel': 55,
                    'speed': 90,
                    'hardBrake': 'yes',
                    'odometer': 45796,
                    'ignition': 'ON'}
        
        return diagnosticsInfo
    
    def getPulse(self):
        
        pulseInfo = {#"type": "diagnostics", 
                    #"subType": "pulse",
                    'odometer': 45796,
                    'fuelLevel': 55}
        
        return pulseInfo
    