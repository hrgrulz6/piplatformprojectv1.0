'''
Created on May 5, 2017

@author: Jackfruit_01
'''
import sqlite3

''' create a connection with the PiEvents DB '''
dbConnection = sqlite3.connect('PiEvents.db')

dbCursor = dbConnection.cursor()

dbCursor.execute('''CREATE TABLE IF NOT EXISTS EventsChain
            (Event text, Chain text)''')

dbCursor.execute("INSERT INTO EventsChain VALUES ('location','getLocation')")
dbCursor.execute("INSERT INTO EventsChain VALUES ('pulse','getLocation,getOdometer,getFuelLevel')")
dbCursor.execute("INSERT INTO EventsChain VALUES ('engine','getDiagnostics')")
dbCursor.execute("INSERT INTO EventsChain VALUES ('driverLog','getLocation,getDiagnostics,getDriverLog')")

dbConnection.commit()

print('EventsChain table created in DB')
