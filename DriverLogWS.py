'''
Created on May 8, 2017

@author: Jackfruit_01
'''
from bottle import run, route, request
from EventReceiver import EventReceiver
import json

class DriverLogWS(object):
    
    eventReceiver = EventReceiver()
    
    def runWebservice(self):
           
        print('web service started')
        
        ''' GET request handler '''
        @route('/getLog', method='GET')
        def getLog():
            print('get log called')

            return 'get log'

        ''' POST request handler '''
        @route('/postLog', method='POST')
        def driverLogInfo():
            logInfo = []
            print('driverLogInfo executed')
            
            info = request.json
            print('info', info)
            #logInfo = json.loads(str(info))
            #defaultCriteria = {'toPost': 'true', 'toCollect':'true'}
            #info.update(defaultCriteria)
            print('post data: ', info)
            logInfo.append(info)
            self.eventReceiver.receiveEvent(logInfo)

        run(host='localhost', port=8181, debug=True)