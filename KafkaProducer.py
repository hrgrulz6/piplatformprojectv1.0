'''
Created on May 6, 2017

@author: Jackfruit_01
'''
from kafka import KafkaProducer
import json
from datetime import datetime

class Kafka_Producer(object):
    
    ''' timestamp '''
    timestamp = datetime.now()
    print('timestamp', timestamp)
    
    producer = KafkaProducer(bootstrap_servers = ["192.168.0.175:9092"])

    def postEventToSpark(self, data):
        
        print('Kafka data: ', data)
        
        eventData = json.dumps(data).encode('utf-8')
        #eventData = data.encode('utf-8')
        
        kafkaResponse = self.producer.send('streamTest', eventData)
        
        print('data sent ', kafkaResponse.get())
        
        response ={'time': str(self.timestamp), 'kafkaResponse':kafkaResponse.get()}
        
        return response