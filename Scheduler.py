'''
Created on May 4, 2017

@author: Jackfruit_01
'''
import schedule, time
from DBData import DbData
from datetime import datetime
from EventProcessor import EventProcessor
from EventReceiver import EventReceiver

class Scheduler(object):
    
    def runScheduler(self):
        
        print('runScheduler started ')
        
        schedule.every(2).seconds.do(self.checkAndProcessData)
        print('schedule.every(1) executed')
        while True:
            schedule.run_pending()
            #time.sleep(1)
            
    ''' current time '''
    currentTime = datetime.now()
    print('currentTime', currentTime)
    
    '''create instance of EventReceiver '''
    eventReceiver = EventReceiver()
        
    def checkAndProcessData(self):
        print('checkAndProcessData executed')
        dbData = DbData()
        eventsScheduleAndStatus = dbData.getEventScheduleAndStatus()
        print('eventsScheduleAndStatus: ', eventsScheduleAndStatus)
        
        listOfPostEventCheck = []
        
        for eachRowData in eventsScheduleAndStatus:
            #print('eachRowData: ', datetime.strptime(eachRowData[3], "%Y-%m-%d %H:%M:%S.%f"))
            postEventCheck = {'toPost': '', 'toCollect':'', 'eventType': '', 'eventSubtype': ''}
            postEventCheck['eventType'] = eachRowData[0]
            postEventCheck['eventSubtype'] = eachRowData[1]
            
            lastPostTime = datetime.strptime(eachRowData[3], "%Y-%m-%d %H:%M:%S.%f")
            print('lastPostTime ', lastPostTime)
            
            lastPostTimeDifference = (self.currentTime - lastPostTime)
            print('lastPostTimeDifference ', lastPostTimeDifference.seconds)
            
            frequencyToPost = eachRowData[2]
            print('frequencyToPost ', frequencyToPost)
            
            if lastPostTimeDifference.seconds >= frequencyToPost:
                #print('post', eachRowData[1])
                postEventCheck['toPost'] = 'true'
                print('post', postEventCheck)
                #listOfPostEventCheck.append(postEventCheck)
            else:
                postEventCheck['toPost'] = 'false'
                print('dont post', postEventCheck)
                #listOfPostEventCheck.append(postEventCheck)
                
            lastCollectTime = datetime.strptime(eachRowData[5], "%Y-%m-%d %H:%M:%S.%f") 
            print('lastCollectTime ', lastCollectTime)  
            
            lastCollectTimeDifference = (self.currentTime - lastCollectTime)
            print('lastCollectTimeDifference ', lastCollectTimeDifference.seconds)
            
            frequencyToCollect = eachRowData[4]
            print('frequencyToCollect ', frequencyToCollect)
            
            if lastCollectTimeDifference.seconds >= frequencyToCollect:
                
                postEventCheck['toCollect'] = 'true'
                print('collect', postEventCheck)
                listOfPostEventCheck.append(postEventCheck)
            else:
                postEventCheck['toCollect'] = 'false'
                print('dont collect', postEventCheck)
                listOfPostEventCheck.append(postEventCheck)
           
                
        print(listOfPostEventCheck)
        
        self.eventReceiver.receiveEvent(listOfPostEventCheck)
            
        
            
            
        
        
        
        