'''
Created on May 4, 2017

@author: Jackfruit_01
'''
import sqlite3, time
from datetime import datetime

class InitialInsertScheduleDataDB(object):
    
    def insertDataToDBInitially(self):

        ''' current time '''
        currentTime = datetime.now()
    
        #print('currentTime: ', currentTime)

        ''' create a connection with the PiEvents DB '''
        dbConnection = sqlite3.connect('PiEvents.db')
        
        dbCursor = dbConnection.cursor()
    
        dbCursor.execute('''CREATE TABLE IF NOT EXISTS SchedulerEvent
            (EventType text, EventSubType text, FrequencyPost int, LastPostedTime text, FrequencyCollect int, LastCollectedTime text)''')
    
        dbCursor.execute("INSERT INTO SchedulerEvent VALUES ('location','location', ?, ?, ?, ?)", (20, currentTime, 10, currentTime))
        
        dbCursor.execute("INSERT INTO SchedulerEvent VALUES ('diagnostics','pulse', ?, ?, ?, ?)", (60, currentTime, 30, currentTime))
        
        dbCursor.execute("INSERT INTO SchedulerEvent VALUES ('diagnostics','engine', ?, ?, ?, ?)", (90, currentTime, 60, currentTime))
        
        dbCursor.execute("INSERT INTO SchedulerEvent VALUES ('driverLog','driverLog', 0, ?, 0, ?)", (currentTime, currentTime))
        
        dbConnection.commit()
        
        checkData = dbCursor.execute('SELECT LastPostedTime FROM SchedulerEvent')
        
        for data in checkData:
            print('data: ', data)
            
        return 'data inserted'