'''
Created on May 6, 2017

@author: Jackfruit_01
'''
from Location import Location
from Diagnostics import Diagnostics
from datetime import datetime

class Capturedata(object):
    
    location = Location()
    diagnostics = Diagnostics()
    
    ''' timestamp '''
    timestamp = datetime.now()
    
    def getData(self, eventChain):
        
        print('getInfo ',eventChain)
        
            
        if eventChain == 'getLocation':
              
            locationInfo = self.location.getLocation()
            eventInfo = {'eventType': 'location', 'eventSubType': 'location', 'timestamp': str(self.timestamp), 'eventData':locationInfo}
            print('locationInfo: ',eventInfo)
            return eventInfo
                
        if eventChain == 'getLocation,getOdometer,getFuelLevel':
            pulseInfo= {}    
            locationInfo = self.location.getLocation()
            odoInfo = self.diagnostics.getPulse()
            pulseInfo.update(locationInfo)
            pulseInfo.update(odoInfo)
            eventInfo = {'eventType': 'diagnostics', 'eventSubType': 'pulse', 'timestamp': str(self.timestamp), 'eventData':pulseInfo}
            print('getLocation,getOdometer,getFuelLevel: ', eventInfo)
            return eventInfo
                
        if eventChain == 'getDiagnostics':
                
            diagInfo = self.diagnostics.getDiagnostics()
            eventInfo = {'eventType': 'diagnostics', 'eventSubType': 'engineStatistics', 'timestamp': str(self.timestamp), 'eventData':diagInfo}
            print('diagInfo: ', eventInfo)
            return eventInfo
        
    def getLogData(self, logInfo):
        
        #if eventChain == 'getLocation,getDiagnostics,getDriverLog':
        print('logInfo ',logInfo)
            
        driverLogInfo = {}
        locationInfo = self.location.getLocation()
        diagInfo = self.diagnostics.getDiagnostics()
        driverLogInfo.update(logInfo)
        driverLogInfo.update(locationInfo)
        driverLogInfo.update(diagInfo)
        
        eventInfo = {'eventType': logInfo.get('eventType'), 'eventSubType': logInfo.get('eventSubtype'), 'timestamp': str(self.timestamp), 'eventData':driverLogInfo}
        print('driverLogInfo ',eventInfo)
        return eventInfo
        
        